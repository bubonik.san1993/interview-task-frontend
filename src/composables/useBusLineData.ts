import { useStore } from "vuex";
import { computed } from "vue";

export const useBusLineData = () => {
    const store = useStore();

    const lines = computed(() => store.getters.linesList);
    const current = computed(() => store.state.activeLine);
    const selectBusLine = (line: number) => store.dispatch("selectBusLine", line);

    return {
        lines,
        current,
        selectBusLine,
    }
}

export const fetchData = () => useStore().dispatch("fetchStops")

