import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import BusLinesTab from "@/components/BusLinesTab.vue";
import StopsTab from "@/components/StopsTab.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'BusLinesTab',
    component: BusLinesTab,
  },
  {
    path: '/stops',
    name: 'StopsTab',
    component: StopsTab,
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
