import { createStore } from "vuex";
import axios from "axios";

export type ScheduledStop = {
  line: number;
  stop: string;
  order: number;
  time: string;
};

type State = {
  loading: boolean;
  stops: ScheduledStop[];
  filteredStops: ScheduledStop[];
  activeLine?: number;
  activeStop?: string;
  stopsSortOrder: boolean;
  maxBlockHeight?: number;
};

const initialState: State = {
  loading: false,
  stops: [],
  filteredStops: [],
  stopsSortOrder: true,
};

export default createStore({
  state: initialState,
  getters: {
    linesList: (state): number[] => {
      const lines = state.stops.map((stop) => stop.line);
      const uniqueLines = [...new Set(lines)];

      return uniqueLines.sort();
    },
    lineInfo: (state): string[] => {
      const lineStops = state.stops.filter(
        (stop) => stop.line === state.activeLine
      );
      const lineInfoByStop = [...new Set(lineStops.map(({ stop }) => stop))];

      lineInfoByStop.sort((a, b) =>
        state.stopsSortOrder ? b.localeCompare(a) : a.localeCompare(b)
      );

      return lineInfoByStop;
    },
    stopInfo: (state) => {
      const activeStopInfo = state.stops.filter(
        ({ stop }) => stop === state.activeStop
      );

      activeStopInfo.sort((stopA, stopB) => {
        const [hoursA, minutesA] = stopA.time.split(":").map(Number);
        const [hoursB, minutesB] = stopB.time.split(":").map(Number);

        if (hoursA === hoursB) {
          return minutesA - minutesB;
        }
        return hoursA - hoursB;
      });

      return activeStopInfo.map((stop) => stop.time);
    },
    stopList: (state): string[] => {
      const uniqueStops = [...new Set(state.stops.map(({ stop }) => stop))];

      return uniqueStops.sort((a, b) =>
        state.stopsSortOrder ? a.localeCompare(b) : b.localeCompare(a)
      );
    },
    filteredStopList: (state): string[] => {
      const uniqueStops = [...new Set(state.filteredStops.map(({ stop }) => stop))];

      return uniqueStops.sort((a, b) =>
        state.stopsSortOrder ? a.localeCompare(b) : b.localeCompare(a)
      );
    },
  },
  mutations: {
    SET_LOADING(state, status: boolean) {
      state.loading = status;
    },
    SET_STOPS(state, stops: ScheduledStop[]) {
      state.stops = stops;
    },
    SET_LINE(state, line: number) {
      state.activeLine = line;
    },
    SET_STOP(state, stop: string) {
      state.activeStop = stop;
    },
    SORT_STOPS(state) {
      state.stopsSortOrder = !state.stopsSortOrder;
    },
    UPDATE_FILTERED_STOPS(state, filtered) {
      state.filteredStops = filtered;
    },
    SET_HEIGHT(state, height: number) {
      state.maxBlockHeight = height;
    }
  },
  actions: {
    fetchStops(context) {
      context.commit("SET_LOADING", true);
      axios.get("http://localhost:3000/stops").then((response) => {
        context.commit("SET_LOADING", false);
        context.commit("SET_STOPS", response.data);
        context.commit("UPDATE_FILTERED_STOPS", response.data);
      }).catch(e => console.error(e));
    },
    selectBusLine(context, line: number) {
      context.commit("SET_LINE", line);
    },
    selectBusStop(context, stop: string) {
      context.commit("SET_STOP", stop);
    },
    handleSort(context) {
      context.commit("SORT_STOPS");
    },
    handleSearch(context, search) {
      const filteredStops = context.state.stops.filter((stop: ScheduledStop) =>
        stop.stop.toLowerCase().includes(search.toLowerCase())
      );
      context.commit("UPDATE_FILTERED_STOPS", filteredStops);
    },
    setMaxHeight(context, maxHeight: number) {
      context.commit("SET_HEIGHT", maxHeight);
    }
  },
  modules: {},
});
